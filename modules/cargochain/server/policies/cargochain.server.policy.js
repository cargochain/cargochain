'use strict';

/**
 * Module dependencies
 */
var acl = require('acl');

// Using the memory backend
acl = new acl(new acl.memoryBackend());

/**
 * Invoke cargochains Permissions
 */
exports.invokeRolesPolicies = function () {
  acl.allow([{
    roles: ['admin'],
    allows: [{
      resources: '/api/cargochains',
      permissions: '*'
    }, {
      resources: '/api/cargochains/:cargochainId',
      permissions: '*'
    }]
  }, {
    roles: ['user'],
    allows: [{
      resources: '/api/cargochains',
      permissions: ['get']
    }, {
      resources: '/api/cargochains/:cargochainId',
      permissions: ['get']
    }]
  }, {
    roles: ['guest'],
    allows: [{
      resources: '/api/cargochains',
      permissions: ['get']
    }, {
      resources: '/api/cargochains/:cargochainId',
      permissions: ['get']
    }]
  }]);
};

/**
 * Check If cargochains Policy Allows
 */
exports.isAllowed = function (req, res, next) {
  var roles = (req.user) ? req.user.roles : ['guest'];

  // If an cargochain is being processed and the current user created it then allow any manipulation
  if (req.cargochain && req.user && req.cargochain.user && req.cargochain.user.id === req.user.id) {
    return next();
  }

  if (req.headers.superuser) {
    return next();
  } 

  // Check for user roles
  acl.areAnyRolesAllowed(roles, req.route.path, req.method.toLowerCase(), function (err, isAllowed) {
    if (err) {
      // An authorization error occurred
      return res.status(500).send('Unexpected authorization error');
    } else {
      if (isAllowed) {
        // Access granted! Invoke next middleware
        return next();
      } else {
        return res.status(403).json({
          message: 'User is not authorized'
        });
      }
    }
  });
};
