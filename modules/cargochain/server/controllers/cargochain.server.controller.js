'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  Cargochain = mongoose.model('Cargochain'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller')),
  lodash = require('lodash');


/**
 * Create a cargochain
 */

exports.create = function (req, res) {
  console.log(req.body);
    if (!req.headers.bulk) {
      var cargochain = new Cargochain(req.body);
      console.log(cargochain);
      cargochain.user = req.user;

      cargochain.save(function (err){
          if (err) {
              return res.status(422).send({
                  message: errorHandler.getErrorMessage(err)
          });
          } else {
              res.json(cargochain);
          }
      })
    }
    else {
      for (var i = 0; i < req.body.data.length; i++) {
        var cargochain = new Cargochain(req.body.data[i]);
        cargochain.user = req.user;
        var if_err = false;
        cargochain.save(function (err){
            if (err) {
                if_err = true;
                return res.status(422).send({
                    message: errorHandler.getErrorMessage(err)
            });
            } 
        })
        if (if_err) {
          break;
        }
      }
      res.json(req.body.data);
    }
}


/**
 * Retrieve the cargochain
 */
exports.read = function (req, res) {
    var cargochain = req.cargochain ? req.cargochain.toJSON() : {};

    cargochain.isCurrentUserOwner = !!(req.user && cargochain.user && cargochain.user._id.toString() === req.user._id.toString());
    res.json(cargochain);
}

/**
 * Update a cargochain
 */
exports.update = function (req, res) {
    var cargochain = req.cargochain;

    lodash.extends(cargochain, req.body);

    cargochain.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cargochain);
    }
  });
}

/**
 * Delete a cargochain
 */
exports.delete = function (req, res) {
  var cargochain = req.cargochain;

  cargochain.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cargochain);
    }
  });
};

/**
 * List of cargochains
 */
exports.list = function (req, res) {
  Cargochain.find().sort('-created').populate('user', 'displayName').exec(function (err, cargochains) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(cargochains);
    }
  });
};


/**
 * Cargochain middleware
 */
exports.cargochainByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'Cargochain id is invalid'
    });
  }

  Cargochain.findById(id).populate('user', 'displayName').exec(function (err, cargochain) {
    if (err) {
      return next(err);
    } else if (!cargochain) {
      return res.status(404).send({
        message: 'No cargochain with that identifier has been found'
      });
    }
    req.cargochain = cargochain;
    next();
  });
};


/**
 * Server longpoll
 */
exports.longPoll = function (req, res) {

  wait(req, res);

}


var wait = function (req, res) {
    var date = new Date();
    var timeout = 1999;
    console.log(req.start);
    // return "push" request every 1 minute
    if (date - req.start > timeout) {
        res.writeHead(200, {
            'Content-Type' : 'text/plain',
            'Access-Control-Allow-Origin' : '*'
        })

        res.write('Push', 'utf8');
        res.end();
    }
    else {
      setTimeout(function() {
            wait(req, res);
          }, timeout);
    }
}

var nodemailer = require('nodemailer');
exports.sendNotification = function (rq, rs) {
 var sendNotification = function(data) {
   var headers = {
     "Content-Type": 'application/json; charset=utf-8',
     "Authorization": 'Basic ODQ1NDkxYjItZDA3NS00OGZmLTk3ZWItMjdmOTY5NzYwZDU1'
   };

   var options = {
     host: 'onesignal.com',
     port: 443,
     path: '/api/v1/notifications',
     method: 'POST',
     headers: headers
   };

   var https = require('https');
   var req = https.request(options, function(res) {
     res.on('data', function(data) {
       console.log("Response:");
       console.log(JSON.parse(data));
     });
   });

   req.on('error', function(e) {
     console.log("ERROR:");
     console.log(e);
   });

   req.write(JSON.stringify(data));
   req.end();
 };

 var message = {
   app_id: "ddb22171-7491-4e91-8968-c01b822cc150",
   contents: { "en": "Your cargo has arrived at the terminal " },
   included_segments: ["All"]
 };
 rs.send("message sent successfully!");
 sendNotification(message);
};

exports.sendMail = function (req, res) {
 var transporter = nodemailer.createTransport({
   service: 'Gmail',
   auth: {
     user: 'chenweilunster@gmail.com',
     pass: 'cwllxyhxd1'
   }
 });
 var text = 'Your cargo has arrived at the terminal \n\n';
 var mailOptions = {
   from: 'chenweilunster@gmail.com',
   to: 'chen897@usc.edu',
   subject: 'Cargochain Notification',
   text: text
 };
 transporter.sendMail(mailOptions, function(err, info) {
   if (err) return res.send(err);
   else {
     res.send('mail sent successfully.');
   }
 });
};