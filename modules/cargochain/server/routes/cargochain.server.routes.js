'use strict';

var cargoPolicy = require('../policies/cargochain.server.policy'),
    cargochains = require('../controllers/cargochain.server.controller');

module.exports = function (app) {
  // Root routing

  app.route('/api/cargochains')
    //.all(cargoPolicy.isAllowed)
    .get(cargochains.list)
    .post(cargochains.create)

  app.route('/api/cargochains/:cargochainId')//.all(cargoPolicy.isAllowed)
    .get(cargochains.read)
    .put(cargochains.update)
    .delete(cargochains.delete)

  app.route('/api/longpolling').all(cargoPolicy.isAllowed)
    .get(cargochains.longPoll)

  app.route('/api/send-email')
    .get(cargochains.sendMail)

  app.route('/api/send-notify')
    .get(cargochains.sendNotification)

  app.param('cargochainId', cargochains.cargochainByID);
};
