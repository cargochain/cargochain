'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var mongoose_timestamp = require('mongoose-timestamp');
/**
 * Article Schema
 */
var CargochainSchema = new Schema({
  /* Field definitions: https://files.slack.com/files-pri/T34P78ZKM-F35GH0PJA/download/scdt_dataset.xlsx */
  id: String,
  vessel_name: String,
  operation_type_name: {type: String, enum: ["inbound", "outbound"]},
  cargo_type: {type: String, enum: ["full", "empty"]},
  quantity: Number,
  shipping_line: String,
  container_nr: String,
  bill_of_lading: String,
  container_size_type: {type: String, enum: ["20", "40", "45"]},
  load_port: String,
  unload_port: String,
  local_or_ipi: {type: String, enum: ["local", "ipi"]},
  inland_point: String,
  agent_voyage_number: String,
  arriving_terminal: String,
  estimated_vessel_arrival: Date,
  cargo_cutoff: Date,
  estimated_ship_departure: Date,
  expected_availability: Date,
  mode_of_entry: {type: String, enum: ["truck", "rail"]},
  mode_of_exit: {type: String, enum: ["truck", "rail"]},

  cargo_status: {type: String, enum: ["pre-arrival", "post-arrival"]},
  data_reporter: {type: String, enum: ["customer", "warehouse", "truck", "port_entry", "truck_unload", "terminla_load", "vessel_loaded", "delivered"]} // Which port reports this Cargo?
});


/* Add create time and update time to the schema. */
CargochainSchema.plugin(mongoose_timestamp, {
  createdAt: 'created_at',
  updatedAt: 'updated_at'
})

mongoose.model('Cargochain', CargochainSchema);
