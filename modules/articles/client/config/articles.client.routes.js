(function () {
  'use strict';

  angular
    .module('articles.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('articles', {
        abstract: true,
        url: '/articles',
        template: '<ui-view/>'
      })
      .state('articles.list', {
        url: '',
        templateUrl: '/modules/articles/client/views/list-articles.client.view.html',
        // controller: 'ArticlesListController',
        // controllerAs: 'vm',
        // data: {
        //   pageTitle: 'Articles List'
        // }
      })
      .state('articles.option', {
        url: '/options',
        templateUrl: '/modules/articles/client/views/view-article.client.options.view.html',
        // controller: 'ArticlesController',
        // controllerAs: 'vm',
        // resolve: {
        //   articleResolve: getArticle
        // },
        // data: {
        //   pageTitle: 'Article {{ articleResolve.title }}'
        // }
      })
      .state('articles.confirm', {
        url: '/confirm',
        templateUrl: '/modules/articles/client/views/view-article.client.confirm.view.html',
        // controller: 'ArticlesController',
        // controllerAs: 'vm',
        // resolve: {
        //   articleResolve: getArticle
        // },
        // data: {
        //   pageTitle: 'Article {{ articleResolve.title }}'
        // }
      })
      .state('articles.result', {
        url: '/result',
        templateUrl: '/modules/articles/client/views/view-article.client.result.view.html',
        // controller: 'ArticlesController',
        // controllerAs: 'vm',
        // resolve: {
        //   articleResolve: getArticle
        // },
        // data: {
        //   pageTitle: 'Article {{ articleResolve.title }}'
        // }
      });
  }

  getArticle.$inject = ['$stateParams', 'ArticlesService'];

  function getArticle($stateParams, ArticlesService) {
    return ArticlesService.get({
      articleId: $stateParams.articleId
    }).$promise;
  }
}());
