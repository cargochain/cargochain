(function () {
  'use strict';

  angular
    .module('customershipment')
    .controller('CustomerShipmentController', CustomerShipmentController);

  CustomerShipmentController.$inject = ['$scope', 'customershipmentResolve', 'Authentication'];

  function CustomerShipmentController($scope, customershipment) {
    var vm = this;

    vm.customershipment = customershipment;
    vm.cargochain = customershipment.cargochain;
  }
}());
