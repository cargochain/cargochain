(function () {
  'use strict';

  angular
    .module('customershipment')
    .controller('CustomerShipmentListController', CustomerShipmentListController);

  CustomerShipmentListController.$inject = ['CustomerShipmentService'];

  function CustomerShipmentListController(CustomerShipmentService) {
    var vm = this;

    vm.customershipment = CustomerShipmentService.query();
  }
}());
