(function (app) {
  'use strict';

  app.registerModule('customershipment', ['core']);// The core module is required for special route handling; see /core/client/config/core.client.routes
  app.registerModule('customershipment.services');
  app.registerModule('customershipment.routes', ['ui.router', 'core.routes', 'customershipment.services']);
}(ApplicationConfiguration));
