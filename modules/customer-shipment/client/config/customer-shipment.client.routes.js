(function () {
  'use strict';

  angular
    .module('customershipment.routes')
    .config(routeConfig);

  routeConfig.$inject = ['$stateProvider'];

  function routeConfig($stateProvider) {
    $stateProvider
      .state('customershipment', {
        abstract: true,
        url: '/customershipment',
        template: '<ui-view/>'
      })
      .state('customershipment.list', {
        url: '',
        templateUrl: '/modules/customer-shipment/client/views/list-customer-shipment.client.view.html',
        controller: 'CustomerShipmentListController',
        controllerAs: 'vm',
        data: {
          pageTitle: 'Customer Shipments List'
        }
      })
      .state('customershipment.view', {
        url: '/:customershipmentId',
        templateUrl: '/modules/customer-shipment/client/views/view-customer-shipment.client.view.html',
        controller: 'CustomerShipmentController',
        controllerAs: 'vm',
        resolve: {
          customershipmentResolve: getCustomerShipment
        },
        data: {
          pageTitle: 'Customer Shipment {{ customershipmentResolve.title }}'
        }
      });
  }

  getCustomerShipment.$inject = ['$http', '$stateParams', 'CustomerShipmentService'];

  function getCustomerShipment($http, $stateParams, CustomerShipmentService) {
    return CustomerShipmentService.get({
      customershipmentId: $stateParams.customershipmentId
    }).$promise
    .then(function(result) {
      return $http.get('/api/cargochains/' + result.cargochain)
      .then(function(httpResult) {
        result.cargochain = httpResult.data;
        return result;
      });
    });
  }
}());
