(function () {
  'use strict';

  angular
    .module('customershipment')
    .run(menuConfig);

  menuConfig.$inject = ['menuService'];

  function menuConfig(menuService) {
    menuService.addMenuItem('topbar', {
      title: 'Customer Shipment',
      state: 'customershipment',
      type: 'dropdown',
      roles: ['*']
    });

    // Add the dropdown list item
    menuService.addSubMenuItem('topbar', 'customershipment', {
      title: 'List Customer Shipments',
      state: 'customershipment.list',
      roles: ['*']
    });
  }
}());
