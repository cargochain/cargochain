(function () {
  'use strict';

  angular
    .module('customershipment.services')
    .factory('CustomerShipmentService', CustomerShipmentService);

CustomerShipmentService.$inject = ['$resource', '$log'];

  function CustomerShipmentService($resource, $log) {
    var CustomerShipment = $resource('/api/customershipment/:customershipmentId', {
      customershipmentId: '@_id'
    }, {
      update: {
        method: 'PUT'
      }
    });

    angular.extend(CustomerShipment.prototype, {
      createOrUpdate: function () {
        var customershipment = this;
        return createOrUpdate(customershipment);
      }
    });

    return CustomerShipment;

    function createOrUpdate(customershipment) {
      if (customershipment._id) {
        return customershipment.$update(onSuccess, onError);
      } else {
        return customershipment.$save(onSuccess, onError);
      }

      // Handle successful response
      function onSuccess(customershipment) {
        // Any required internal processing from inside the service, goes here.
      }

      // Handle error response
      function onError(errorResponse) {
        var error = errorResponse.data;
        // Handle error internally
        handleError(error);
      }
    }

    function handleError(error) {
      // Log error
      $log.error(error);
    }
  }
}());
