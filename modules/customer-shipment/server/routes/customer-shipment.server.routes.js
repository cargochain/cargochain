'use strict';

var //cargoPolicy = require('../policies/cargochain.server.policy'),
    customershipment = require('../controllers/customer-shipment.server.controller');

module.exports = function (app) {
  // Root routing

  app.route('/api/customershipment')
      //.all(cargoPolicy.isAllowed)
    .get(customershipment.list)
    .post(customershipment.create)

  app.route('/api/customershipment/:customershipmentId')//.all(cargoPolicy.isAllowed)
    .get(customershipment.read)
    .put(customershipment.update)
    .delete(customershipment.delete)

  app.param('customershipmentId', customershipment.customershipmentByID);
};
