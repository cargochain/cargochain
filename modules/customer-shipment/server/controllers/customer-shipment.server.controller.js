'use strict';

/**
 * Module dependencies
 */
var path = require('path'),
  mongoose = require('mongoose'),
  CustomerShipment = mongoose.model('CustomerShipment'),
  errorHandler = require(path.resolve('./modules/core/server/controllers/errors.server.controller'));


/**
 * Create a customer_shipment
 */

exports.create = function (req, res) {
    console.log(req.body);
    var customershipment = new CustomerShipment(req.body);
    // article.user = req.user;

    customershipment.save(function (err){
        if (err) {
            return res.status(422).send({
                message: errorHandler.getErrorMessage(err)
        });
        } else {
            res.json(customershipment);
        }
    })
}


/**
 * Retrieve the customer_shipment
 */
exports.read = function (req, res) {
    var customershipment = req.customershipment ? req.customershipment.toJSON() : {};

    //article.isCurrentUserOwner = !!(req.user && article.user && article.user._id.toString() === req.user._id.toString());
    res.json(customershipment);
}

/**
 * Update a cargochain
 */
exports.update = function (req, res) {
    var customershipment = req.customershipment;

    /*  Model specific update
    cargochain.
    */

    customershipment.save(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(customershipment);
    }
  });
}

/**
 * Delete a customershipment
 */
exports.delete = function (req, res) {
  var customershipment = req.customershipment;

  customershipment.remove(function (err) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(customershipment);
    }
  });
};

/**
 * List of customershipment
 */
exports.list = function (req, res) {
  CustomerShipment.find().sort('-created').populate('user', 'displayName').exec(function (err, customershipment) {
    if (err) {
      return res.status(422).send({
        message: errorHandler.getErrorMessage(err)
      });
    } else {
      res.json(customershipment);
    }
  });
};


/**
 * Cargochain middleware
 */
exports.customershipmentByID = function (req, res, next, id) {

  if (!mongoose.Types.ObjectId.isValid(id)) {
    return res.status(400).send({
      message: 'CustomerShipment is invalid'
    });
  }

  CustomerShipment.findById(id).populate('user', 'displayName').exec(function (err, customershipment) {
    if (err) {
      return next(err);
    } else if (!customershipment) {
      return res.status(404).send({
        message: 'No customershipment with that identifier has been found'
      });
    }
    req.customershipment = customershipment;
    next();
  });
};
