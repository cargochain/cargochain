'use strict';

/**
 * Module dependencies
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

/**
 * CustomerShipment Schema
 */
var CustomerShipmentSchema = new Schema({
  created: {
    type: Date,
    default: Date.now
  },
  shipmentId: {
    type:String,
    required: true
  },
  shipFrom: {
    country: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    state: {
      type: String
    },
    zipCode: {
      type: String,
      required: true
    }
  },
  shipTo: {
    country: {
      type: String,
      required: true
    },
    city: {
      type: String,
      required: true
    },
    state: {
      type: String
    },
    zipCode: {
      type: String,
      required: true
    }
  },
  info: {
    // Simplified: Hazardous, count, unit, etc
    length: { type: Number, required: true },
    width: { type: Number, required: true },
    height: { type: Number, required: true },
    weight: { type: Number, required: true },
    price: { type: Number, required: true }
  },
  /*shipmentHistory: [{
    description: String,
    timestamp: Date
  }],*/
  cargochain: {
    type: Schema.ObjectId,
    ref: 'Cargochain'
  },
  /*user: {
    type: Schema.ObjectId,
    ref: 'User'
  }*/
});

mongoose.model('CustomerShipment', CustomerShipmentSchema);
