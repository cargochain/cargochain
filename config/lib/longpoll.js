'use strict';

/**
 * Module dependencies.
 */
var config = require('../config'),
  mongoose = require('./mongoose'),
  express = require('./express'),
  chalk = require('chalk'),
  seed = require('./seed'),
  http = require('http');

var pollports = [8124, 8125, 8126];
var timeout = 59999;

pollports.forEach(function(port){
    http.createServer(function (req, res){
        waitUpdateRequest(req, res);
    }).listen(port);
})

function waitUpdateRequest(req, res) {
    var date = new Date();
    // if it's been waiting for a long time, return the response
    if (date - request.socket._idelStart.getTime() > timeout) {
        res.writeHead(200, {
            'Content-Type' : 'text/plain',
            'Access-Control-Allow-Origin' : '*'
        })

        res.write('Again', 'utf8');
        res.end();
    }

    // let the 


}

