const fs = require('fs');
const querystring = require('querystring');
var http = require('http');

var db_file = fs.openSync("../data/data.json", "r");
var db_object = JSON.parse(fs.readFileSync(db_file));
var myhostname = "localhost"
var myport = 3000;

var next_id = db_object.length + 1;

var push_all_entries = function() {
	var options = {
		hostname: myhostname,
		port: myport,
		path: '/api/cargochains',
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'superuser': true,
			'Transfer-Encoding': 'chunked',
			'bulk': true
		}
	};
	var req = http.request(options, (res) => {
			console.log(`STATUS: ${res.statusCode}`);
			console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
			res.setEncoding('utf8');
			res.on('data', (chunk) => {
				console.log(`BODY: ${chunk}`);
			});
			res.on('end', () => {
				console.log('No more data in response.');
			});
		});

	req.on('error', (e) => {
		console.log(`problem with request: ${e.message}`);
	});
	 
	// only write 10 objects
	// var ii = 0;
	// var limit = 10
	// for (obj in db_object) {
	// 	req.write(obj);
	// 	if (ii++ >= limit) {
	// 		break;
	// 	}
	// }

	var postData = {
		data: db_object.slice(0,10)
	};
	console.log(`postData: ${JSON.stringify(postData)}`)
	req.write(JSON.stringify(postData), 'utf8');
	req.end();
	return;
	
}
/* Returns an entry given an id */
exports.query = function(id) {
	for (i in db_object) {
		if (db_object[i]["ID"] == id) {
			console.log(db_object[i]);
			return;
		}
	}
	console.log("Item not found.");
};

/* For demonstration purpose, add a predefined item to the database. */
exports.add = function() {
	var itemToAdd = {
		"Bill of Lading #": "9999999999",
		"ID": next_id,
		"Expected Availability": "",
		"Mode of Exit": "",
		"Estimated Ship Departure": 1726876800.0,
		"Agent Voyage Number": "1W",
		"Quantity": 1.0,
		"Operation Type Name": "Outbound",
		"Inland Point": "Example Example",
		"Vessel Name": "Example",
		"Unload Port": "Example Example",
		"Container Size Type": "40",
		"Shipping Line": "Example Examples",
		"Cargo Cutoff": 1726012800.0,
		"Arriving Terminal": "",
		"Local or IPI": "Local",
		"Load Port": "Example Example",
		"Cargo Type": "Full Container",
		"Container #": "HKLU9876543",
		"Mode of Entry": "Truck",
		"Estimated Vessel Arrival": 1726531200.0
	}

	console.log("Item with ID " + next_id + " inserted.")
	next_id = next_id + 1;

	db_object.push(itemToAdd);

	// TODO: Change the following data according to API
	var postData = querystring.stringify({
		'cargochain' : db_object[i]
	});

	var postPath = '/api/cargochains/';
	postPath += id.toString();

	var options = {
		hostname: myhostname,
		port: myport,
		path: postPath,
		method: 'POST',
		headers: {
			'Content-Type': 'text/plain',
			'Content-Length': Buffer.byteLength(postData),
			'superuser': true
		}
	};

	var req = http.request(options, (res) => {
		console.log(`STATUS: ${res.statusCode}`);
		console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
		res.setEncoding('utf8');
		res.on('data', (chunk) => {
			console.log(`BODY: ${chunk}`);
		});
		res.on('end', () => {
			console.log('No more data in response.');
		});
	});

	req.on('error', (e) => {
		console.log(`problem with request: ${e.message}`);
	});

	req.write(postData);
	req.end();
	return;
}

/* For demonstration purpose, modify item with given id's VESSEL NAME to "Example". */
exports.modify = function(id) {
	for (i in db_object) {
		if (db_object[i]["ID"] == id) {
			db_object[i]["Vessel Name"] = "Example";

			// TODO: Change the following data according to API
			var postData = querystring.stringify({
				'cargochain' : db_object[i]
			});

			var putPath = '/api/cargochains/';
			putPath += id.toString();

			var options = {
				hostname: myhostname,
				port: myport,
				path: putPath,
				method: 'PUT',
				headers: {
					'Content-Type': 'text/plain',
					'Content-Length': Buffer.byteLength(postData),
					'superuser': true
				}
			};

			var req = http.request(options, (res) => {
				console.log(`STATUS: ${res.statusCode}`);
				console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
				res.setEncoding('utf8');
				res.on('data', (chunk) => {
					console.log(`BODY: ${chunk}`);
				});
				res.on('end', () => {
					console.log('No more data in response.');
				});
			});

			req.on('error', (e) => {
				console.log(`problem with request: ${e.message}`);
			});

			req.write(postData);
			req.end();
			return;
		}
	}
	console.log("Item not found.");
}

/* Delete entry with certain ID */
exports.delete = function(id) {
	for (i in db_object) {
		if (db_object[i]["ID"] == id) {

			var deletePath = '/api/cargochains/';
			deletePath += id.toString();

			var options = {
				hostname: myhostname,
				port: myport,
				path: deletePath,
				method: 'DELETE',
				headers: {
					'Content-Type': 'text/plain',
					'Content-Length': Buffer.byteLength(postData),
					'superuser': true
				}
			};

			var req = http.request(options, (res) => {
				console.log(`STATUS: ${res.statusCode}`);
				console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
				res.setEncoding('utf8');
				res.on('data', (chunk) => {
					console.log(`BODY: ${chunk}`);
				});
				res.on('end', () => {
					console.log('No more data in response.');
				});
			});

			req.on('error', (e) => {
				console.log(`problem with request: ${e.message}`);
			});

			req.write(postData);
			req.end();
			return;
		}
	}
	console.log("Item not found.");
}

// TODO: Change the following data according to long polling API
var longpoll_fn = function() {

	var options = {
		hostname: myhostname,
		port: myport,
		path: '/api/longpolling',
		method: 'GET',
		headers: {
		'Content-Type': 'text/plain',
		'superuser': true
		}
	};

	var req = http.request(options, (res) => {
		console.log(`STATUS: ${res.statusCode}`);
		console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
		res.setEncoding('utf8');
		res.on('data', (chunk) => {
			console.log(`BODY: ${chunk}`);

			// // TODO: if it returns with heartbeat, the call this function again.
			// if (...) {
			// 	longpoll_fn();
			// }
		// TODO: if it returns with a push request, push all data.
		if (chunk == 'Push') {
			push_all_entries();
			longpoll_fn();
		}
		});
		res.on('end', () => {
			console.log('No more data in response.');
		});
	});

	req.on('error', (e) => {
		console.log(`problem with request: ${e.message}`);
	});

	req.setTimeout(1200000, function() {
		longpoll_fn();
	})

	req.end();
};

longpoll_fn();
