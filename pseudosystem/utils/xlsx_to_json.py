import sys
import xlrd
import json

workbook = xlrd.open_workbook("../data/Hackathon-Data-post-arrival.xlsx")
sheet = workbook.sheet_by_index(0)

if sheet.nrows == 0:
	print("Sheet is empty")
	sys.exit(1)

# Convert Excel datetime to Unix timestamp
for row_index in range(1, sheet.nrows):
	for col_index in range(sheet.ncols):
		if sheet.cell(row_index, col_index).ctype == xlrd.XL_CELL_DATE:
			excel_datetime = sheet.cell(row_index, col_index).value
			unix_timestamp = (excel_datetime - 22569) * 86400
			sheet.put_cell(row_index, col_index, xlrd.XL_CELL_NUMBER, unix_timestamp, None)

header = sheet.row_values(0)

data_list = [{'_'.join(col_name.lower().split()): col_data for col_name, col_data in zip(header, sheet.row_values(i))} for i in range(1, sheet.nrows)]

json_file = open("../data/data.json", "w")
json_file.write(json.dumps(data_list, indent=2))



